# -*- coding: utf-8 -*-
##############################################################################
#
#
##############################################################################

from trytond.pool import Pool
from .configuration import *
from .health_contact_tracing_followup_fiuner import *
from .health_eno import *

from .wizard import *

from .report import *


def register():
    Pool.register(
        Configuration,
        Campaign,
        ConfigurationContext,
        Context,       
        ContactTracingENOMedicament,
        ContactTracing,
        ContactTracingCall,
        ContactTracingSymptomsCheck,
        ContactTracingParty,
        ContactTracingMembers,
        CreateImportCsvStart,
        CreateImportCsvPreview,
        CreateImportCsvContactData,
        ContactTracingUser,
        ENO,
        ENODrugTreatment,
        ContactTracingENOParty,
        CreateFromENOStart,
        CreateFromENOPatientList,
        CreateFromENOAssignUsers,
        CreateContactTracingReportsStart,
        module='health_contact_tracing_followup_fiuner', type_='model')
    Pool.register(
        CreateImportCsvWizard,
        CreateFromENOWizard,
        CreateContactTracingReportsWizard,
        module='health_contact_tracing_followup_fiuner', type_='wizard')
    Pool.register(
        CallReport,
        PatientFollowingReport,
        GeoreffReport,
        ENONotificationReport,
        DischargeReport,
        ContactTracingStatsReport,
        IsolationReport,
    	module='health_contact_tracing_followup_fiuner', type_='report')
