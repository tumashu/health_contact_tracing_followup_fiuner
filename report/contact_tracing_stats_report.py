# -*- coding: utf-8 -*-
 
from trytond.pool import Pool
from trytond.report import Report

from datetime import datetime
from dateutil.relativedelta import relativedelta

__all__ = ['ContactTracingStatsReport']

class ContactTracingStatsReport(Report):
    'Geo Referentiation Report'
    __name__ = 'gnuhealth.contact_tracing.stats.report'
    
    @classmethod
    def get_pop(cls, vlist, campaign, lower_limit, upper_limit):
        now = datetime.now()
        res =\
            [x for x in vlist\
                if (x.campaign.id == campaign) and\
                x.patient.dob and\
                lower_limit<=relativedelta(now,datetime.combine(x.patient.dob,datetime.min.time())).years<=upper_limit]
        print('*'*20+'\n',[x.patient.age for x in res], lower_limit,upper_limit)
        return res

    @classmethod
    def get_context(cls, records, data):
        pool = Pool()
        ContactTracing = pool.get('gnuhealth.contact_tracing')
        context = super(ContactTracingStatsReport, cls).get_context(records, data)
        
        campaign = data['campaign'] 
        start = data['start']
        end = data['end']
        pathology = data['pathology']
        suspected_pathology = data['suspected_pathology']
        now = datetime.now()
        contact_tracings = ContactTracing.search([
            ('first_contact','>=',start),
            ('first_contact','<=',end)
            ])
        context['objects'] = contact_tracings
        
        categories = ['positive','suspected','close_contact']
        #positive epidemiological_condition == 'confirmed' or swabbing_result == 'positive' or virus identified
        #suspected epidemiological_condition == 'suspected' or virus identified
        #'close_contact' epidemiological_condition == ('close_contact' or None)
        for cat in categories:
            '''total and total by gender'''
            context[cat+'_total'] =\
                [x for x in contact_tracings
                    if (x.campaign.id == campaign)
                    and ((cat =='positive' and (x.pathology and x.pathology.id == pathology))
                    or (cat =='suspected' and (x.pathology and x.pathology.id == suspected_pathology))
                    or (cat =='close_contact' and (x.pathology and x.pathology.id not in [pathology, suspected_pathology]\
                        or x.pathology == None)))]
            context[cat+'_masculin'] =\
                [x for x in context[cat+'_total']\
                    if x.patient.gender == 'm']
            context[cat+'_femenin'] =\
                [x for x in context[cat+'_total']\
                    if x.patient.gender == 'f']
        
            ''' total by  Discharge State'''
            context[cat+'_following'] =\
                [x for x in context[cat+'_total']                  
                    if not x.discharge]
            context[cat+'_discharge'] =\
                [x for x in context[cat+'_total']
                    if x.discharge]
            '''Discharge results'''
            discharge_results = [['recuperated','r'],['hospitalized','h'],['death','d'],['lost','l']]
            for dr in discharge_results:
                context[cat+'_discharge_'+dr[0]] =\
                    [x for x in context[cat+'_total']\
                    if x.discharge and x.result == dr[1]]
        ''' Age period '''
        #population with an arbitrary age step
        indicators_range = [[0,14],[15,39],[40,64],[65,150]]
        #population with a step of 5
        piramid_ranges = [[x,x+4] for x in range(0,90,5)]
        piramid_ranges.append([90,120])
        
        #go trough all the ranges
        for x in indicators_range+piramid_ranges:
            lower = str(x[0])
            upper = str(x[1])
            population = cls.get_pop(contact_tracings, campaign, x[0], x[1])
            male_pop = '_'.join(['population','m',lower,upper])
            female_pop = '_'.join(['population','f',lower,upper])
            context[male_pop] = [x for x in population if x.patient.gender == 'm']
            context[female_pop] = [x for x in population if x.patient.gender == 'f']
            categories = ['positive','suspected','close_contact']
            #go through each category
            for cat in categories:
                total_pop_in_step = '_'.join([cat,lower,upper])
                male_pop_in_cat = '_'.join([cat,'m',lower,upper])
                female_pop_in_cat = '_'.join([cat,'f',lower,upper])
                if cat == 'positive':
                    context[total_pop_in_step] =\
                        [x for x in population\
                            if (x.pathology and x.pathology.id == pathology)]
                elif cat == 'suspected':
                    context[total_pop_in_step] =\
                        [x for x in population\
                            if (x.pathology and x.pathology.id == suspected_pathology)]
                elif cat == 'close_contact':
                    context[total_pop_in_step] =\
                        [x for x in population\
                            if (x.pathology and x.pathology.id not in [pathology,suspected_pathology]
                                or x.pathology is None)]
                context[male_pop_in_cat] =\
                    [x for x in context[total_pop_in_step]\
                        if x.patient.gender == 'm']
                context[female_pop_in_cat] =\
                    [x for x in context[total_pop_in_step]\
                        if x.patient.gender == 'f']
        return context
