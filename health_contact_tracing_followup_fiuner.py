# -*- coding: utf-8 -*-
##############################################################################
#
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import urllib.parse
import sys

from trytond.model import ModelView, ModelSQL, Workflow, fields, Unique
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Or, Eval, Not, Bool, Equal
from trytond import backend
from dateutil.relativedelta import relativedelta
from datetime import datetime, timedelta, date
from trytond.transaction import Transaction
from trytond.tools import reduce_ids, grouped_slice
from sql import *
from sql.aggregate import *

__all__ = ['ContactTracing', 'ContactTracingCall', 'ContactTracingSymptomsCheck',
           'ContactTracingParty','ContactTracingUser',
           'ContactTracingMembers']


class ContactTracing(metaclass=PoolMeta):
    'First Contacts with the Person'
    __name__ = 'gnuhealth.contact_tracing'
    
    state = fields.Selection([
        (None,''),
        ('in_process','In process'),
        ('done','Done'),
        ],'State',sort=False,readonly=True)
    campaign = fields.Many2One('gnuhealth.contact_tracing.campaign','Campaign',
            states={
                'readonly': Eval('state')=='done',
                },
            required=True)
    institutions = fields.Function(
        fields.Many2Many('company.company',None,None,'Institutions'),
        'get_institutions',searcher="search_institutions")
    first_caller  = fields.Many2One('gnuhealth.healthprofessional','Caller',
            states={
                'readonly': Eval('state')=='done',
                })
    critical_info = fields.Function(
        fields.Text('Critical Info'),
        'get_critical_info')
    age = fields.Function(
        fields.Char('Age'),
        'get_age')
    contact_tracings = fields.One2Many('gnuhealth.contact_tracing_call',
        'name','Contacts Tracings',
        states={
            'readonly': Eval('state')=='done',
            })
    first_contact = fields.DateTime('First Contact Date',required=True,
        states={
            'readonly': Eval('state')=='done',
            })
    expected_discharge_date = fields.DateTime('Expected Discharge Date',
        states={
            'readonly': Eval('state')=='done',
            })
    discharge = fields.Boolean('Person Discharge',
        states={
            'readonly': Eval('state')=='done',
            })
    result = fields.Selection([
        (None, ''),    
        ('r', 'recuperated'),
        ('h', 'hospitalized'),
        ('dl', 'discarded by laboratory'),
        ('l', 'tracing lossed'),
        ('d', 'death person'),
        ('cca', 'asymptomatic close contact'),
        ], 'Result', help="Discharge result "
        "Discharge Result", sort=False,
        states={
            'invisible': Not(Bool(Eval('discharge'))),
            'readonly': Eval('state')=='done',
            })
    result_string = result.translated('result')
    discharge_date = fields.DateTime('Discharge Date', 
        states={
            'invisible': Not(Bool(Eval('discharge'))),
            'readonly': Eval('state')=='done',
            })
    health_prof = fields.Many2One('gnuhealth.healthprofessional', 'Health Professional',
        states={
            'invisible': Not(Bool(Eval('discharge'))),
            'readonly': Eval('state')=='done',
            })
    phone = fields.Function(
        fields.Char('Phone',
                    states={
                        'readonly': Eval('state')=='done',
                    }), 
        'get_party_phone', setter='set_party_phone')
    #members = fields.Many2Many('gnuhealth.contact_tracing-party.party',
        #'contact_tracing', 'party', 'Contact Members')
    responsible_users = fields.Many2Many(
        'gnuhealth.contact_tracing-res.user',
        'contact_tracing','user','Responsible Users',
        states={
            'readonly': Eval('state')=='done',
            },
        help='Users that can access the record')
    call_icon = fields.Function(
        fields.Selection([
            (None,''),
            ('to-call','To call'),
            ('already-call','Already call'),
            ('tracing-done','Tracing done'),
            ],'Call icon'),'get_call_icon',searcher="search_call_icon")
    urladdr = fields.Function(
        fields.Char(
            'OSM Map',help="Locates the DU on the Open Street Map by default"),
            'on_change_with_urladdr')
    urladdr2 = fields.Function(
        fields.Char('Google Maps', help="Locates the DU on Google Maps"),
        'on_change_with_google_maps_url')    
    geo_ref = fields.Function(
        fields.Selection([
            (None,''),
            ('no-georef','No georef'),
            ('georef','Georef'),
            ],'Geo Ref'),'get_geo_ref',searcher="search_geo_ref")
    members_reached = fields.Function(
        fields.Many2Many('gnuhealth.contact_tracing.members',
                        None,'party',
                        'Members on du'),
                        'get_members_reached_on_du')
    members_unreached = fields.Function(
        fields.Many2Many('gnuhealth.contact_tracing.members',
                        None,'party',
                        'Members on du'),
                        'get_members_unreached_on_du')
    context_selection = fields.Many2One('gnuhealth.contact_tracing.context','Context',
        domain=[('id','in',Eval('context_list'))],
        states={
            'readonly': Eval('state')=='done',
            })
    context_list = fields.Function(
        fields.Many2Many('gnuhealth.contact_tracing.context',None,None,'Context List'),
        'get_context_list')
    epidemiological_condition = fields.Selection([
        (None,''),
        ('confirmed','Confirmed'),
        ('suspect','Suspect'),
        ('close_contact','Close contact'),
        ],'Epidemiological Condition',sort=False,
        states={
            'readonly': Eval('state')=='done',
            })

    def get_context_list(self, name):
        pool = Pool()
        Config = pool.get('gnuhealth.contact_tracing.configuration')(0)
        if Config.contexts:
            return [x.id for x in Config.contexts if x.active]
        
    swabbing_date = fields.Date('Swabbing date',
        states={
            'readonly': Eval('state')=='done',
            })
    swabbing_result = fields.Selection([
        (None,''),
        ('positive','+'),
        ('negative','-'),
        ],'Swabbing result',
        states={
            'readonly': Eval('state')=='done',
            })
    eno = fields.Many2One('gnuhealth.contact_tracing.eno','Mandatory notification disease',
        states={
            'readonly': Eval('state')=='done',
            })
    suspect_criteria = fields.Function(
        fields.Selection([
            (None,''),
            ('gnuhealth-warning','Criteria 1'),
            ],'Suspect criteria'),
        'get_suspect_criteria',searcher='search_suspect_criteria')
        
    '''Delta Times'''
    deltatime_detection_contact = fields.Function(
        fields.Integer('Time Det-Cont'), 'get_deltatime_detection_contact')
    deltatime_detection_discharge = fields.Function(
        fields.Integer('Time Det-Disch'), 'get_deltatime_detection_discharge')
    deltatime_contact_discharge = fields.Function(
        fields.Integer('Time Cont-Disch'), 'get_deltatime_contact_discharge')
    deltatime_contact_expected_discharge = fields.Function(
        fields.Integer('Time Cont-Exp Disch'), 'get_deltatime_contact_expected_discharge')
    deltatime_expected_discharge_discharge = fields.Function(
        fields.Integer('Time Exp Disch-Disch'), 'get_deltatime_expected_discharge_discharge')
    
    def get_institutions(self,name):
        if self.responsible_users:
            return ([x.company.id for x in self.responsible_users])    
    
    def get_call_icon(self, name):
        if self.contact_tracings and not self.discharge:
            call_dates = [x.date for x in self.contact_tracings if x.date]
            start_today = (datetime.now()-timedelta(hours=3)).replace(hour=0,minute=0,second=0)
            if call_dates:
                last_call = max(call_dates) - timedelta(hours=3)
                if last_call>start_today:
                    return 'already-call'
        if self.discharge:
            return 'tracing-done'
        return 'to-call'
    
    @fields.depends('du')
    def on_change_with_urladdr(self,name = None):
        if self.du:
            return self.du.urladdr
        return ''
    
    @fields.depends('du')
    def on_change_with_google_maps_url(self, name=None):
        if self.du:
            lang = Transaction().language[:2]
            url = ' '.join([
                self.du.address_street or '',str(self.du.address_street_number or ''),
                self.du.address_city or '',
                self.du.address_subdivision and self.du.address_subdivision.name or '',
                self.du.address_country and self.du.address_country.name or ''])
            if url.strip():
                return 'http://maps.google.com/maps?hl=%s&q=%s' % \
                    (lang, urllib.parse.quote(url))
        return ''
    
    def get_geo_ref(self,name):
        if self.du and self.du.latitude and self.du.longitude:
            return 'georef'
    
    def get_members_reached_on_du(self,name):
        pool = Pool()
        ContactTracing = pool.get('gnuhealth.contact_tracing')
        res = []
        if self.du and self.du.members:
            contactTracing_patients = ContactTracing.search([
                                ('patient.name','in',self.du.members),
                                ])            
            res = [party.id for party in self.du.members if (party.id != self.patient.name.id\
                        and party.id in [cT.patient.name.id for cT in\
                            [x for x in contactTracing_patients if x.discharge==False]])]
        return (res)

    def get_members_unreached_on_du(self,name):
        pool = Pool()
        ContactTracing = pool.get('gnuhealth.contact_tracing')
        res = []
        if self.du and self.du.members:
            contactTracing_patients = ContactTracing.search([
                                ('patient.name','in',self.du.members),
                                ])            
            res = [party.id for party in self.du.members if (party.id != self.patient.name.id\
                        and party.id not in [cT.patient.name.id for cT in contactTracing_patients])]
        return (res)
    
    def get_suspect_criteria(self,name):
        if self.contact_tracings:
            for call in self.contact_tracings:            
                res = [call.anosmia,call.arthralgia,call.diarrhea_vomit,
                    call.dysgeusia,call.dyspnoea,call.fever_high,call.myalgia,
                    call.cough,call.odynophagia,call.headache]
                if len([x for x in res if x =='yes'])>1 or\
                    call.dysgeusia == 'yes' or call.anosmia == 'yes':
                    return 'gnuhealth-warning'
        return None
        
    def get_deltatime_detection_contact(self, name):
        if self.contact_date and self.first_contact:
            delta = relativedelta(self.first_contact, self.contact_date)
            return int(delta.days)

    def get_deltatime_detection_discharge(self, name):
        if self.contact_date and self.discharge_date:
            delta2 = relativedelta(self.discharge_date, self.contact_date)
            return int(delta2.days)

    def get_deltatime_contact_discharge(self, name):
        if self.first_contact and self.discharge_date:
            delta3 = relativedelta(self.discharge_date, self.first_contact)
            return int(delta3.days)

    def get_deltatime_contact_expected_discharge(self, name):
        if self.contact_date and self.expected_discharge_date:
            delta4 = relativedelta(self.expected_discharge_date, self.contact_date,)
            return int(delta4.days)

    def get_deltatime_expected_discharge_discharge(self, name):
        if self.discharge_date and self.expected_discharge_date:
            delta5 = relativedelta(self.discharge_date, self.expected_discharge_date)
            return int(delta5.days)

    @fields.depends('patient')
    def on_change_with_phone(self, name=None):
        return self.get_party_phone([self])[self.id]

    @classmethod
    def get_party_phone(cls, contact_tracing, name=None):
        result = {}
        for a in contact_tracing:
            result[a.id] = a.patient and a.patient.name.phone or ''
        return result

    @classmethod
    def set_party_phone(cls, contact_tracing, name, value):
        pool = Pool()
        ContactMechanism = pool.get('party.contact_mechanism')
        for a in contact_tracing:
            if not a.patient:
                continue
            party_phone = ContactMechanism.search([
                ('party', '=', a.patient.name),
                ('type', '=', 'phone'),
                ])
            if party_phone:
                ContactMechanism.write(party_phone, {
                    'value': value,
                    })
            else:
                ContactMechanism.create([{
                    'party': a.patient.name.id,
                    'type': 'phone',
                    'value': value,
                    }])

    @classmethod
    def search_institutions(cls,name,clause):
        res = []
        value = clause[2]
        res.append(('responsible_users.company',clause[1],value))
        return res

    @classmethod
    def search_call_icon(cls, name, clause):
        transaction = Transaction()
        connection = transaction.connection
        cursor = connection.cursor()

        pool = Pool()
        ContactTracing = pool.get('gnuhealth.contact_tracing')
        ContactTracingCall = pool.get('gnuhealth.contact_tracing_call')

        contactTracing = ContactTracing.__table__()
        contactTracingCall = ContactTracingCall.__table__()
        
        #set a threshold for use on queries
        start_today = (datetime.now()-timedelta(hours=3)).replace(hour=0,minute=0,second=0)
        
        result0 = []
        result1 = []

        if clause[2] == 'already-call' or clause[2] == 'to-call':
            #Bring all the id's that are still tracing
            query0 =(contactTracing.select(contactTracing.id,
                    where=(contactTracing.discharge=='false')))
            cursor.execute(*query0)
            result0 = cursor.fetchall()
            #make a list for those id's
            #Bring the ids that are not called since today
            #The reduce_ids acts like a IN sentence
            query1 = (contactTracingCall.select(contactTracingCall.name,
                    where=(reduce_ids(contactTracingCall.name,[x[0] for x in result0])
                            &(contactTracingCall.date>=start_today)
                            )))
            cursor.execute(*query1)
            result1 = cursor.fetchall()
            if clause[2] == 'to-call':
                #Eliminates all the id's that are common on both set list'
                result1 = list(set(result0)^set(result1))
        elif clause[2] == 'tracing-done':
            #Bring all the discharge contact tracings
            query1 = (contactTracing.select(contactTracing.id,
                    where=(contactTracing.discharge=='true')))
            cursor.execute(*query1)
            result1 = cursor.fetchall()
        return [('id','in',[x[0] for x in result1])]

    @classmethod
    def search_geo_ref(cls, name, clause):
        pool = Pool()
        transaction = Transaction()
        connection = transaction.connection
        cursor = connection.cursor()
        
        ContactTracing = pool.get('gnuhealth.contact_tracing')
        DU = pool.get('gnuhealth.du')

        contactTracing = ContactTracing.__table__()
        du = DU.__table__()

        result0 = []
        result1 = []
        if clause[2] == 'no-georef' or clause[2] == 'georef':
            query0 = (du.select(du.id,
                        where=(du.latitude!=None)
                            &(du.longitude!=None)
                            ))
            cursor.execute(*query0)
            result0 = cursor.fetchall()
            query1 = (contactTracing.select(contactTracing.id,
                            where=(reduce_ids(contactTracing.du,[x[0] for x in result0]))))
            cursor.execute(*query1)
            result1 = cursor.fetchall()
            if clause[2]== 'georef':
                pass
            elif clause[2]=='no-georef':
                query2 = (contactTracing.select(contactTracing.id))
                cursor.execute(*query2)
                result2 = cursor.fetchall()
                result1 = list(set(result1)^set(result2))
        return [('id','in',[x[0] for x in result1])]
    
    @classmethod
    def search_suspect_criteria(cls, name, clause):
        pool = Pool()
        transaction = Transaction()
        connection = transaction.connection
        cursor = connection.cursor()
        
        ContactTracingCall = pool.get('gnuhealth.contact_tracing_call')

        cTCall = ContactTracingCall.__table__()

        result0 = []
        if clause[2] == 'gnuhealth-warning':
            query0 = (cTCall.select(cTCall.name,
                        where=(((
                            (cTCall.arthralgia == 'yes').cast('INTEGER') +\
                            (cTCall.diarrhea_vomit == 'yes').cast('INTEGER') +\
                            (cTCall.dyspnoea == 'yes').cast('INTEGER') +\
                            (cTCall.fever_high == 'yes').cast('INTEGER') +\
                            (cTCall.myalgia == 'yes').cast('INTEGER') +\
                            (cTCall.cough == 'yes').cast('INTEGER') +\
                            (cTCall.odynophagia == 'yes').cast('INTEGER') +\
                            (cTCall.headache == 'yes').cast('INTEGER')
                                ) >= Literal(2))
                            |(cTCall.dysgeusia == 'yes')
                            |(cTCall.anosmia == 'yes')
                            )))
            cursor.execute(*query0)
            result0 = cursor.fetchall()
        return [('id','in',[x[0] for x in result0])]

    def get_critical_info(self, name):
        return self.on_change_with_critical_info()
    
    @fields.depends('patient')
    def on_change_with_critical_info(self):
        if (self.patient and self.patient.critical_summary):
            return (self.patient.critical_summary)
        return None

    def get_age(self,name):
        return self.on_change_with_age()        
        
    @fields.depends('patient','first_contact')
    def on_change_with_age(self):
        if self.patient and self.patient.dob and self.first_contact:
            return str(self.first_contact.year - self.patient.dob.year)
        return ''

    @fields.depends('patient','du')
    def on_change_with_members(self):        
        if self.patient and self.du:
            parties = self.du.members
            return ([x.id for x in parties if x.id != self.patient.name.id])
        return None
    
    @fields.depends('patient')
    def on_change_with_du(self):
        if self.patient and self.patient.name.du:
            return self.patient.name.du.id
        return None
    
    @fields.depends('discharge')
    def on_change_with_discharge_date(self):
        if self.discharge:
            return datetime.now()
        return None
    
    @staticmethod
    def default_campaign():
        pool = Pool()
        Configuration = pool.get('gnuhealth.contact_tracing.configuration')(1)
        if Configuration.current_campaign:
            return int(Configuration.current_campaign)

    @staticmethod
    def default_first_caller():
        pool = Pool()
        HealthProfessional = pool.get('gnuhealth.healthprofessional')
        return HealthProfessional.get_health_professional()

    @staticmethod
    def default_first_contact():
        now = datetime.now()
        return now

    @staticmethod
    def default_status():
        return 'na'

    @staticmethod
    def default_exposure_risk():
        return 'na'
    
    @staticmethod
    def default_responsible_users():
        pool = Pool()        
        Group = pool.get('res.group')        
        group = []
        context = {'language':'en'}
        users = []
        with Transaction().set_context(context):
            group = Group.search([('name','=','Health Contact Tracing Administration')])
            users = group[0].users
        return ([x.id for x in users])
    
    @staticmethod
    def default_state():
        return 'in_process'    
    
    @classmethod
    def search_rec_name(cls, name, clause):
        if clause[1].startswith('!') or clause[1].startswith('not '):
            bool_op = 'AND'
        else:
            bool_op = 'OR'
        return [bool_op,
            ('patient.rec_name',) + tuple(clause[1:]),           
            ]

    @classmethod
    def __setup__(cls):
        super(ContactTracing, cls).__setup__()
        #t = cls.__table__()
        cls._error_messages.update({
            'patient_currently_contacted': 'The patient is currently contacted. Check the records'
            })
        cls._buttons.update({
                'done':{
                    'invisible': Or(Bool(Eval('state')=='done'),Not(Eval('discharge'))),
                    },
            })
        cls.patient.states={'readonly': Eval('state')=='done'}
        cls.pathology.states={'readonly': Eval('state')=='done'}
        cls.contact.states={'readonly': Eval('state')=='done'}
        cls.contact_date.states={'readonly': Eval('state')=='done'}
        cls.du.states={'readonly': Eval('state')=='done'}
        cls.operational_sector.states={'readonly': Eval('state')=='done'}
        cls.exposure_risk.states={'readonly': Eval('state')=='done'}
        cls.status.states={'readonly': Eval('state')=='done'}
        cls.context.states={'readonly': Eval('state')=='done'}
        cls.comments.states={'readonly': Eval('state')=='done'}

    @classmethod
    @ModelView.button
    def done(cls, vlist):
        cls.write(vlist,{
                'state': 'done',
                })

    @classmethod
    def create(cls, vlist): 
        return super(ContactTracing, cls).create(vlist)
    
    @classmethod
    def validate(cls, contact_tracings):
        super(ContactTracing, cls).validate(contact_tracings)
        for ct in contact_tracings:
            ct.check_patient_current_contact_tracing()

    def check_patient_current_contact_tracing(self):
        ''' Check for only one current contact tracing '''
        contact_tracing = Table('gnuhealth_contact_tracing')
        cursor = Transaction().connection.cursor()
        patient_id = int(self.patient.id)
        cursor.execute (*contact_tracing.select(Count(contact_tracing.patient),
            where=(contact_tracing.discharge == 'false') &
            (contact_tracing.patient == patient_id))) 
        records = cursor.fetchone()[0]
        if records > 1:
            self.raise_user_error('patient_currently_contacted')


class ContactTracingCall(ModelSQL, ModelView,Workflow):
    'Person Contact Tracing'
    __name__ = 'gnuhealth.contact_tracing_call'

    name = fields.Many2One('gnuhealth.contact_tracing','Contact Tracing')    
    caller_tracing  = fields.Many2One('gnuhealth.healthprofessional','Caller')    
    date = fields.DateTime('Date')        
    temp = fields.Float('Temperatura')    
    satO2 = fields.Integer('Oxigen Saturation')    
    heart_rate = fields.Integer('Heart Rate')    
    pressure_diastolic = fields.Integer('Diastolic')    
    pressure_systolic = fields.Integer('Systolic')    
    respiratory_rate = fields.Integer('Respiratory Rate')    
    symptoms = fields.One2Many('gnuhealth.contact_tracing.symptoms_check','name','Symptoms Check')    
    evolution = fields.Selection([
        (None, ''),
        ('na','Not answered'),
        ('n', 'Status Quo'),
        ('i', 'Improving'),
        ('w', 'Worsening'),
        ],'How do you consider your health since yesterday?', help="Evolution", 
        sort=False, required=True)
    evolution_string = evolution.translated('evolution')
    introspective_health = fields.Selection([
        (None,''),
        ('na','Not answered'),
        ('bad','Bad'),
        ('regular','Regular'),
        ('good','Good'),
        ('very_good','Very good'),
        ('excelent','Excelent'),
        ],"How do you consider your health today?",sort=False,required=True,
        help="Instrospective sense of health")
    introspective_health_string = introspective_health.translated('introspective_health')
    evolution_number = fields.Function(
        fields.Integer('Evolution'),
            'get_evolution_number')
    status = fields.Selection((
        (None, ''),  
        ('unreached', 'Unreached'),
        ('followingup', 'Following up'),
        ('na', 'Not available'),
        ), 'Status', sort=False,
        help="Unreached: The contact has not been reached yet."
        "\nFollowing up: The contact has been traced, demographics information"
        " has been created and followup evaluations status are stored in the"
        " evaluations")
    status_string = status.translated('status')
    alert = fields.Boolean('Alert')    
    verbal = fields.Boolean('Verbal')    
    pain = fields.Boolean('Pain')    
    awareness = fields.Boolean('Awareness')    
    notes = fields.Text('Notes')
    anosmia = fields.Selection([
        (None,''),
        ('yes','Yes'),
        ('no','No'),
        ],'Do you have a loss of smell?',sort=False,
        help="Anosmia",required=True)
    anosmia_string = anosmia.translated('anosmia')
    arthralgia = fields.Selection([
        (None,''),
        ('yes','Yes'),
        ('no','No'),
        ],'Do you have joint pain?',sort=False,
        help="Arthhralgia ", required=True)
    arthralgia_string = arthralgia.translated('arthralgia')
    diarrhea_vomit = fields.Selection([
        (None,''),
        ('yes','Yes'),
        ('no','No'),
        ],'Do you have diarrhea and vomiting?',sort=False,
        required=True,
        help='¿Presenta ambos síntomas?')
    diarrhea_vomit_string = diarrhea_vomit.translated('diarrhea_vomit')
    dysgeusia = fields.Selection([
        (None,''),
        ('yes','Yes'),
        ('no','No'),
        ],'Do you have loss of taste?',sort=False,
        help="Disgeusia",required=True)
    dysgeusia_string = dysgeusia.translated('dysgeusia')
    dyspnoea = fields.Selection([
        (None,''),
        ('yes','Yes'),
        ('no','No'),
        ],'Do you have respiratory distress?',sort=False,
        help="Dispnoea",required=True)
    dyspnoea_string = dyspnoea.translated('dyspnoea')
    fever_high = fields.Selection([
        (None,''),
        ('yes','Yes'),
        ('no','No'),
        ],"Do you have a fever over 37.5?",sort=False,
        required=True)
    fever_high_string = fever_high.translated('fever_high')
    myalgia = fields.Selection([
        (None,''),
        ('yes','Yes'),
        ('no','No'),
        ],"Do you have muscle aches?",sort=False,
        help="Myalgia",required=True)
    myalgia_string = myalgia.translated('myalgia')
    cough = fields.Selection([
        (None,''),
        ('yes','Yes'),
        ('no','No'),
        ],"Do you have a cough?",sort=False,
        required=True)
    cough_string = cough.translated('cough')
    odynophagia = fields.Selection([
        (None,''),
        ('yes','Yes'),
        ('no','No'),
        ],"Do you have sore throat?",sort=False,
        help="Odynophagia ",required=True)
    odynophagia_string = odynophagia.translated('odynophagia')
    headache = fields.Selection([
        (None,''),
        ('yes','Yes'),
        ('no','No'),
        ],"Do you have a headache?",sort=False,
        required=True)
    headache_string = headache.translated('headache')
    has_no_symptoms = fields.Boolean('No symptoms')
    has_signs = fields.Boolean('Signs')
    
    warning_icon = fields.Function(
        fields.Char('Warning Call'),'get_warning_icon')

    @fields.depends('status')
    def on_change_status(self):
        if self.status in ['unreached','na']:
            self.evolution = 'na'
            self.introspective_health = 'na'
            self.alert = False
            self.verbal = False
            self.pain = False
            self.awareness = False
            self.anosmia = 'no' 
            self.arthralgia = 'no'
            self.diarrhea_vomit = 'no'
            self.dysgeusia = 'no'
            self.dyspnoea = 'no'
            self.fever_high = 'no'
            self.myalgia = 'no'
            self.cough = 'no'
            self.odynophagia = 'no'
            self.headache = 'no'  
            self.has_no_symptoms = True
        else:
            self.evolution = None
            self.introspective_health = None
            self.anosmia = None
            self.arthralgia = None
            self.diarrhea_vomit = None
            self.dysgeusia = None
            self.dyspnoea = None
            self.fever_high = None
            self.myalgia = None
            self.cough = None
            self.odynophagia = None
            self.headache = None
            self.has_no_symptoms = False

    @fields.depends('has_no_symptoms')
    def on_change_has_no_symptoms(self):
        if self.has_no_symptoms:
            self.alert = False
            self.verbal = False
            self.pain = False
            self.awareness = False
            self.anosmia = 'no' 
            self.arthralgia = 'no'
            self.diarrhea_vomit = 'no'
            self.dysgeusia = 'no'
            self.dyspnoea = 'no'
            self.fever_high = 'no'
            self.myalgia = 'no'
            self.cough = 'no'
            self.odynophagia = 'no'
            self.headache = 'no'
        else:
            self.anosmia = None
            self.arthralgia = None
            self.diarrhea_vomit = None
            self.dysgeusia = None
            self.dyspnoea = None
            self.fever_high = None
            self.myalgia = None
            self.cough = None
            self.odynophagia = None
            self.headache = None
    
    def get_evolution_number(self,name):
        if self.evolution == 'w':
            return 10
        if self.evolution == 'n':
            return 20
        if self.evolution == 'i':
            return 30
        return None
    
    def get_warning_icon(self, name):
        return self.on_change_with_warning_icon()
        
    @fields.depends('anosmia','arthralgia','diarrhea_vomit','dysgeusia',
        'dyspnoea','fever_high','myalgia','cough','odynophagia','headache')
    def on_change_with_warning_icon(self):
        res = [self.anosmia,self.arthralgia,self.diarrhea_vomit,
            self.dysgeusia,self.dyspnoea,self.fever_high,self.myalgia,
            self.cough,self.odynophagia,self.headache]
        if len([x for x in res if x =='yes'])>1 or self.dysgeusia == 'yes' or\
            self.anosmia == 'yes':
            return 'gnuhealth-warning'
        else:
            return None
    
    @classmethod
    def view_attributes(cls):
        return [('/form/group[@id="contact_signs"]', 'states', {
                    'invisible':Not(Bool(Eval('has_signs'))),
                })]
    
    @staticmethod
    def default_caller_tracing():
        pool = Pool()
        HealthProfessional = pool.get('gnuhealth.healthprofessional')
        return HealthProfessional.get_health_professional()
    
    @staticmethod
    def default_date():
        now = datetime.now()
        return now


class ContactTracingSymptomsCheck(ModelSQL, ModelView):
    'Person Symptoms'
    __name__ = 'gnuhealth.contact_tracing.symptoms_check'

    name = fields.Many2One('gnuhealth.contact_tracing_call','Contact Tracing')    
    symptom = fields.Many2One('gnuhealth.pathology', 'Symptom')
    
    # TODO PARA LA SEGUNDA VERSION PODEMOS AGREGAR UNA LISTA DE SINTOMAS SEGUN LA ENFERMAD A CHEQUEAR


class ContactTracingParty(ModelSQL):
    'Contact Tracing Member'
    __name__ = 'gnuhealth.contact_tracing-party.party'

    party = fields.Many2One('party.party','Party',required=True, ondelete='CASCADE')
    contact_tracing = fields.Many2One('gnuhealth.contact_tracing',
        'Contact Tracing',required=True, ondelete='CASCADE')


class ContactTracingUser(ModelSQL):
    'Contact Tracing - Health Professional'
    __name__ = 'gnuhealth.contact_tracing-res.user'

    user = fields.Many2One(
        'res.user','User',required=True, ondelete='CASCADE')
    contact_tracing = fields.Many2One('gnuhealth.contact_tracing',
        'Contact Tracing', required=True, ondelete='CASCADE')


class ContactTracingMembers(ModelView):
    'Contact Tracing Members'
    __name__ = 'gnuhealth.contact_tracing.members'

    party = fields.Many2One('party.party','Party')
